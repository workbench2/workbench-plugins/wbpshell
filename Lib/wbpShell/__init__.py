
from .preferences import ShellPreferences
from .shellwin import shellPanel

__version__ = "0.2.0"


panels = [shellPanel]
preferencepages = [ShellPreferences]
