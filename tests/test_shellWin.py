from wbBase.application import App
from wbBase.applicationInfo import ApplicationInfo, PluginInfo

appinfo = ApplicationInfo(
    Plugins=[PluginInfo(Name="shell", Installation="default")]
)



def test_plugin():
    app = App(test=True, info=appinfo)
    assert "shell" in app.pluginManager
    app.Destroy()

def test_shellwin_instance():
    app = App(test=True, info=appinfo)
    from wbpShell.shellwin import Shell
    shellwin = Shell(app.TopWindow)
    assert isinstance(shellwin, Shell)
    app.Destroy()
