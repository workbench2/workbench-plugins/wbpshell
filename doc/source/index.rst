
Welcome to the wbpShell documentation
==============================================================================

wbpShell is a plugin for `WorkBench <https://workbench2.gitlab.io/wbbase/>`_ 
applications. It adds a shell panel to your application where you can execute
arbitrary python statements within the namespace of the running application. 

.. toctree::
   :maxdepth: 2
   :caption: Content:

   shortcuts
   wbpShell

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
