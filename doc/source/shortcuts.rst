
Keyboard shortcuts
==============================================================================

======================== =============================================================
Key                       Action
======================== =============================================================
:kbd:`Home`              Go to the beginning of the command or line.
:kbd:`Shift+Home`        Select to the beginning of the command or line.
:kbd:`Shift+End`         Select to the end of the line.
:kbd:`End`               Go to the end of the line.
:kbd:`Ctrl+C`            Copy selected text, removing prompts.
:kbd:`Ctrl+Shift+C`      Copy selected text, retaining prompts.
:kbd:`Alt+C`             Copy to the clipboard, including prefixed prompts.
:kbd:`Ctrl+X`            Cut selected text.
:kbd:`Ctrl+V`            Paste from clipboard.
:kbd:`Ctrl+Shift+V`      Paste and run multiple commands from clipboard.
:kbd:`Ctrl+Up-Arrow`     Retrieve Previous History item.
:kbd:`Alt+P`             Retrieve Previous History item.
:kbd:`Ctrl+Down-Arrow`   Retrieve Next History item.
:kbd:`Alt+N`             Retrieve Next History item.
:kbd:`Shift+Up-Arrow`    Insert Previous History item.
:kbd:`Shift+Down-Arrow`  Insert Next History item.
:kbd:`F8`                Command-completion of History item.

                         (Type a few characters of a previous command and press F8.)
:kbd:`Ctrl+Enter`        Insert new line into multiline command.
:kbd:`Ctrl+]`            Increase font size.
:kbd:`Ctrl+[`            Decrease font size.
:kbd:`Ctrl+=`            Default font size.
:kbd:`Ctrl+Space`        Show Auto Completion.
:kbd:`Ctrl+Alt+Space`    Show Call Tip.
:kbd:`Shift+Enter`       Complete Text from History.
:kbd:`Ctrl+F`            Search
:kbd:`F3`                Search next
:kbd:`Ctrl+H`            "hide" lines containing selection / "unhide"
:kbd:`F12`               on/off "free-edit" mode
======================== =============================================================

