wbpShell package
================

.. automodule:: wbpShell
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   wbpShell.preferences
   wbpShell.shellwin
